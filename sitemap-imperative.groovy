
final t     = System.currentTimeMillis()
final lines = 'http://lenta.ru/sitemap.xml'.toURL().text.readLines()

for ( int j = 0; j < lines.size(); j++ )
{
    final line = lines[ j ].trim()
    if ( line.startsWith( '<loc>' ) && line.endsWith( '</loc>' ))
    {
        final url = line[ 5 .. -7 ]
        if ( url.contains( '/sport/' ))
        {
            url.toURL().text
            println "[$url]"
        }
    }
}

println "[${ System.currentTimeMillis() - t }] ms"