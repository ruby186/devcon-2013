
// findAll, grep

println ( 'Even numbers   0 .. 1000  = ' + ( 0 .. 1000 ).findAll { it % 2 == 0 })
println ( '7-multiplies in 10 .. 30  = ' + ( 0 .. 1000 ).step( 7 ).grep( 10 .. 30 ))
println ( 'Non-empty lines in a file = ' + new File( 'functions.groovy' ).text.readLines()*.trim().grep())

// collect, *.op()

println ( 'Number length  0 .. 1000  = ' + ( 0 .. 1000 ).collect { it.toString() }.collect { it.length() })
println ( 'Number length  0 .. 1000  = ' + ( 0 .. 1000 )*.toString()*.length())

// inject

println ( 'Sum of numbers 0 .. 1000  = ' + ( 0 .. 1000 ).sum())
println ( 'Max number     0 .. 1000  = ' + ( 0 .. 1000 ).max())
println ( 'Numbers        0 .. 1000  = ' + ( 0 .. 1000 ).join( '-' ))

println ( 'Sum of numbers 0 .. 1000  = ' + ( 0 .. 1000 ).inject( 0  ) { int sum,  int n -> sum + n })
println ( 'Max number     0 .. 1000  = ' + ( 0 .. 1000 ).inject( 0  ) { int max,  int n -> n > max ? n : max  })
println ( 'Numbers        0 .. 1000  = ' + ( 0 .. 1000 ).inject( '' ) { String s, int n -> s ? "$s-$n" : "$n" })

// closures

final factorsOf   = { int N    -> ( 1 .. N ).findAll { N % it == 0 }}
final isPrime     = { int N    -> factorsOf( N ).size() < 3 }
final wrap        = { Object o -> "[$o]" }
final printPrimes = {
    println ( 'Prime numbers  2 .. 1000  = ' + ( 2 .. 1000 ).findAll( isPrime ).
                                                             collect( wrap ).
                                                             join( '' ))
}

// Template design pattern in FP

final timer = {
    Closure c ->
    final t = System.currentTimeMillis()
    c()
    println "[${ System.currentTimeMillis() - t }] ms"
}

// Running closures

printPrimes.call()
printPrimes()
timer { printPrimes() }
timer ( printPrimes )
