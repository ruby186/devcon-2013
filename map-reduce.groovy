
// Improving Map's sort()

class MyCategory
{
    static String sort ( Map<String,?> map )
    {
        final keys       = map.keySet()
        final keyPadSize = keys*.size().max() + 3

        keys.sort().
             collect { String key -> "[$key]".padRight( keyPadSize ) + ":[${ map[ key ] }]" }.
             join( '\n' )
    }
}

println System.properties.sort()
use( MyCategory ){ println System.properties.sort()}


/*
// Reading nmap output

println '/usr/local/bin/nmap -v yandex.ru'.
        execute().text.readLines().
        findAll { String line -> line ==~ ~/\d+\/\w+\s+open\s+.+$/ }.
        collect { String line -> line.find ( /(\d+)/ ){ it[ 1 ] }}
*/


// Calculating size of all groovy files

println 'ls -al'.
        execute().text.readLines().
        collect { String       line -> line.split( /\s+/ ).toList() }.
        findAll { List<String> l    -> l.size() == 9 }.
        findAll { List<String> l    -> l[ -1 ].endsWith( '.groovy' ) }.
        collect { List<String> l    -> l[ 4 ] as int }.
        sum()


// Reading non-comment lines

println new File( '.gitignore' ).text.
        readLines().
        grep().
        findAll { ! it.startsWith( '#' ) }


// Converting a multi-line String into Map

println '''
        zip    = z1|z2
        tar    = t1|t2|t3
        tar.gz = tz'''.tokenize( '\n' )*.
                       trim().
                       grep().
                       inject( [:].withDefault{ [] }){
                            Map m, String line ->
                            def ( String format, String extensions ) = line.tokenize( '=' )*.trim()
                            (( List ) m[ format ] ).addAll( extensions.tokenize( '|' )*.trim().grep())
                            m
                       }
