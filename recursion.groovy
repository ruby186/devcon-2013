
int sum1 ( int N )
{
    int result  = 0
    for ( int n = 1; n <= N; n++ ){ result += n }
    result
}


int sum2 ( int N, int n = 0, int sum = 0 )
{
    ( n <= N ) ? sum2( N, n + 1, sum + n ) : sum
}


assert sum1 ( 100 ) == 5050
assert sum1 ( 100 ) == sum2( 100 )


long fSomething ( int N )
{
    def a = 0
    def b = 1
    def c

    for ( int n = 0; n < N; n++ )
    {
        c = a + b
        b = a
        a = c
    }

    a
}


long fib ( int N )
{
    ( N < 2 ) ? N : fib( N - 1 ) + fib( N - 2 )
}


assert fSomething ( 36 ) == 14930352
assert fSomething ( 36 ) == fib ( 36 )
