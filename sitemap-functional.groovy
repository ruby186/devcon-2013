
final timer = {
    Closure c ->
    final t = System.currentTimeMillis()
    c()
    println "[${ System.currentTimeMillis() - t }] ms"
}

timer {
    'http://lenta.ru/sitemap.xml'.
    toURL().text.readLines()*.
    trim().
    findAll{ String s -> s.startsWith( '<loc>' ) && s.endsWith( '</loc>' )}.
    collect{ String s -> s[ 5 .. -7 ] }.
    findAll{ String s -> s.contains( '/sport/' ) }.
    each   { String s -> s.toURL().text; println "[$s]" }
}
