import groovyx.gpars.GParsPool

final timer = {
    Closure c ->
    final t = System.currentTimeMillis()
    c()
    println "[${ System.currentTimeMillis() - t }] ms"
}

final range            = ( 2 .. 10000 )
final isPrime          = { int N -> ( 1 .. N ).findAll{ N % it == 0 }.size() < 3 }
final printRangePrimes = { println ( range.findAll( isPrime )) }

timer { GParsPool.withPool { println ( range.findAllParallel( isPrime )) }}

timer { GParsPool.withPool { range.asConcurrent( printRangePrimes ) }}

timer ( printRangePrimes )

